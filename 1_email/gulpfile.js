var gulp          = require('gulp'),
    sass          = require('gulp-sass'),
    pug           = require('gulp-pug'),
    autoprefixer  = require('gulp-autoprefixer'),
    plumber       = require('gulp-plumber'),
    browserSync   = require('browser-sync').create(),
    imagemin      = require('gulp-imagemin'),
    concat        = require('gulp-concat'),
    rename        = require('gulp-rename'),
    uglify        = require('gulp-uglify'),
    replace = require('gulp-replace');
    inlineCss = require('gulp-inline-css'),

    appSass       = 'app/assets/styles/app.sass',
    sassDir       = 'app/assets/styles',
    cssDir        = 'dist/css',
    
    appImg        = 'app/assets/images',
    imgDir        = 'dist/img',
    
    appPug        = 'app/views/pages',
    
    jsDir         = 'app/assets/scripts/**/*.js',
    jsDest        = 'dist/js';

gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: 'dist'
    }
  })
});

gulp.task('scripts', function() {
  return gulp.src(jsDir)
      .pipe(concat('app.js'))
      .pipe(gulp.dest(jsDest))
      .pipe(rename('app.min.js'))
      .pipe(uglify())
      .pipe(gulp.dest(jsDest))
      .pipe(browserSync.reload({stream: true}));
});

gulp.task('sass', function(){
  return gulp.src(appSass)
      .pipe(plumber())
      .pipe(sass().on('error', sass.logError))
      .pipe(autoprefixer({browsers: ['last 15 versions']}))
      .pipe(gulp.dest(cssDir))
      .pipe(browserSync.reload({stream: true}))
});

gulp.task('pug', function() {
  return gulp.src(appPug+'/*.pug')
      .pipe(pug())
      .pipe(gulp.dest('./dist'))
      .pipe(browserSync.reload({stream: true}));
});

gulp.task('html', function () {
  return gulp.src('./dist/*.html')
    .pipe(inlineCss({
      applyStyleTags: false,
      removeHtmlSelectors: true
    }))
    .pipe(gulp.dest('./dist/result'));
});

gulp.task('images', function(){
  return gulp.src(appImg+'/**/*.+(png|jpg|gif|svg)')
      .pipe(imagemin())
      .pipe(gulp.dest(imgDir))
      .pipe(browserSync.reload({stream: true}))
});

gulp.task('watch:styles', function () {
  gulp.watch(sassDir, gulp.series('sass'));
});

gulp.task('watch:pug', function () {
  gulp.watch('app/views/**/*.pug', gulp.series('pug'));
});

gulp.task('watch:scripts', function () {
  gulp.watch('app/assets/scripts/**/*.js', gulp.series('scripts'));
});

gulp.task('watch:images', function () {
  gulp.watch('app/assets/images/**/*.+(png|jpg|gif|svg)', gulp.series('images'));
});

gulp.task('watch:html', function () {
  gulp.watch('./dist/*.html', gulp.series('html'));
});

gulp.task('watch', gulp.series(['sass', 'pug', 'images', 'scripts', 'html'],
    gulp.parallel('watch:pug', 'watch:styles', 'watch:images', 'watch:scripts', 'watch:html')
));

gulp.task('default', gulp.series(['sass', 'pug', 'images', 'scripts', 'html'],
    gulp.parallel('watch', 'browserSync')
));
