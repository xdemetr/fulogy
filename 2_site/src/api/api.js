export const productAPI = {
  getProduct() {
    return {
      id: 1,
      details: [
        {
          label: 'Класс',
          value: 'Standart',
          detailId: 'det01',
          type: 'primary'
        },
        {
          label: 'Потребляемая мощность',
          value: '59 Вт.',
          detailId: 'det02'
        },
        {
          label: 'Сила света',
          value: '3459 Люмен = 7,5 ламп накаливания по 40 Вт.',
          detailId: 'det03'
        },
        {
          label: 'Гарантия',
          value: '3 года',
          detailId: 'det04'
        },
        {
          label: 'Монтаж',
          value: 'Да',
          detailId: 'det05'
        },
        {
          label: 'Итого сумма:',
          value: '2594 руб.',
          detailId: 'det06'
        }
      ],
      gallery: [
        {
          id: 11,
          title: 'Теплый',
          previewImage: 'thumb_01.jpg',
          images: ['01.jpg', '02.jpg', '03.jpg'],
        },
        {
          id: 12,
          title: 'Дневной',
          previewImage: 'thumb_02.jpg',
          images: ['04.jpg', '05.jpg', '06.jpg'],
        },
        {
          id: 13,
          title: 'Холодный',
          previewImage: 'thumb_03.jpg',
          images: ['07.jpg', '08.jpg', '09.jpg'],
        }
      ]
    }
  }
};
