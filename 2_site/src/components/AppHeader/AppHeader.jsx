import React from 'react';
import styles from './AppHeader.module.sass';

const AppHeader = () => {
  return (
    <div className={styles.header}>
      <div className={styles.container}>
        <div className={styles.logo}></div>
      </div>
    </div>
  );
};

export default AppHeader;
