import React from 'react';

import AppHeader from '../AppHeader';
import Product from '../Product';

const App = () => {
  return (
    <>
      <AppHeader/>
      <Product/>
    </>
  );
};

export default App;
