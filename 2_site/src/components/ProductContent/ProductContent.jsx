import React from 'react';

import styles from './ProductContent.module.sass';
import cn from 'classnames';

const ProductContent = ({ product, onSelectGallery, setInfoVisible }) => {
  if (!product.current) return null;

  const onSelectGalleryItem = (id) => {
    onSelectGallery(id);
  };

  const infoHandle = () => {
    setInfoVisible()
  };

  const { current, selectedImage } = product;

  const details = current.details.map(({ label, value, detailId, type }) => {
    return (
      <dl
        key={detailId}
        className={cn(styles.details, type === 'primary' && styles['details_primary'])}>
        <dt className={styles['details__label']}>{label}</dt>
        <dd className={styles['details__value']}>
          <span>{value}</span>
        </dd>
      </dl>
    )
  });

  const gallery = current.gallery.map(({ title, previewImage, id }) => {
    return (
      <div
        className={cn(
            styles['galleryPreview__item'],
            selectedImage === id && styles['galleryPreview__item_selected'])}
        key={id}
        onClick={() => onSelectGalleryItem(id)}
      >
        <img
          className={styles['galleryPreview__image']}
          src={`http://fb.joomdesign.ru/fulogy/${previewImage}`} alt={title}/>
        <span className={styles['galleryPreview__title']}>{title}</span>
      </div>
    )
  });

  return (
    <>
      {details}

      <div className={styles.lightInfo}>
        <button
          onClick={infoHandle}
          className={styles['lightInfo__button']}>info</button>
        <span className={styles['lightInfo__text']}>Выберите цвет свечения</span>
      </div>

      <div className={styles.galleryPreview}>
        {gallery}
      </div>
    </>
  );
};

export default ProductContent;
