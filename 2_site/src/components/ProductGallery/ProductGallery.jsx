import React from 'react';
import Swiper from 'react-id-swiper';

import 'swiper/css/swiper.css';
import styles from './ProductGallery.module.sass';

const ProductGallery = ({ product }) => {
  if (!product.current) return null;

  const { current, selectedImage } = product;
  const gallery = current.gallery.filter(el => el.id === selectedImage)[0];

  const galleryItems = gallery.images.map((item, idx) => {
    return (
      <div className={styles['gallery__item']}>
        <img
          className={styles['gallery__image']}
          src={`http://fb.joomdesign.ru/fulogy/${item}`} alt=""/>
      </div>
    )
  });

  const swiperParams = {
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true
    }
  };

  return (
    <div className={styles.gallery}>
      <Swiper {...swiperParams}>
        {galleryItems}
      </Swiper>
    </div>
  );
};

export default ProductGallery;
