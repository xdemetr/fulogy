import React, { useEffect, useState } from 'react';
import ProductContent from '../ProductContent/ProductContent';
import ProductGallery from '../ProductGallery';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { getProduct, selectGalleryItem } from '../../store/product-reducer';
import cn from 'classnames';

import styles from './Product.module.sass';

const Product = ({ product, getProduct, selectGalleryItem }) => {
  useEffect(() => {
    getProduct()
  }, [getProduct]);

  const [infoVisible, setInfoVisible] = useState(false);

  if (!product) return null;

  const handleInfo = () => {
    setInfoVisible(!infoVisible);
  };

  return (
    <div className={styles.product}>
      <div className={styles['product__gallery']}>
        <ProductGallery product={product}/>
      </div>
      <div className={styles['product__content']}>
        <ProductContent
          product={product}
          onSelectGallery={selectGalleryItem}
          setInfoVisible={handleInfo}
        />
      </div>

      <div className={cn(styles['product__light-info'], infoVisible && styles['product__light-info_visible'])}>
        <span className={styles['product__light-link-back']} onClick={handleInfo}>Вернуться</span>
        <p>Светодиодная лента 24v SMD 2835 196 led/m способна излучать ровный, достаточно мощный свет. Высокий индекс
          цветопередачи (CRI 85 - 90) означает, что все освещенные предметы будут выглядеть так, как при дневном
          свете.</p>
        <p>Открытые светодиодные ленты являются не влагозащищенными, поэтому рекомендуется эксплуатировать их в
          сухих помещениях. На одном метре платы находится 196 светодиодов повышенной яркости, обеспечивающих мощный
          световой поток в 1800 люмен, что равносильно лампе накаливания 100 Ватт.</p>
        <p>Ленты изготовлены на основе гибкой печатной платы со скотчем “3М” на обратной стороне, что позволяет легко
          производить монтаж без дополнительных крепежей.</p>
        <p>Перед монтажом важно правильно рассчитать мощность блока питания для светодиодной ленты от ее общей длины.
          Если светодиодную ленту подключить к блоку питания меньшего напряжения, то свечение будет в лучшем случае
          тусклым.</p>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  product: state.products,
});

export default compose(connect(mapStateToProps, { getProduct, selectGalleryItem }))(Product);
