import { productAPI } from '../api/api';

const FETCH_PRODUCT_REQUEST = 'FETCH_PRODUCT_REQUEST';
const FETCH_PRODUCT_SUCCESS = 'FETCH_PRODUCT_SUCCESS';
const PRODUCT_GALLERY_SELECT = 'PRODUCT_GALLERY_SELECT';

let initialState = {
  loading: false,
  current: null,
  selectedImage: 11
};

const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PRODUCT_REQUEST: {
      return {
        ...state,
        loading: true
      }
    }

    case FETCH_PRODUCT_SUCCESS: {
      return {
        ...state,
        loading: false,
        current: action.current
      }
    }

    case PRODUCT_GALLERY_SELECT: {
      return {
        ...state,
        selectedImage: action.selectedImage
      }
    }

    default:
      return state;
  }
};

const productLoading = () => ({ type: FETCH_PRODUCT_REQUEST });
const productLoaded = (current) => ({ type: FETCH_PRODUCT_SUCCESS, current });

export const selectGalleryItem = (selectedImage) => ({ type: PRODUCT_GALLERY_SELECT, selectedImage });

export const getProduct = () => async dispatch => {
  dispatch(productLoading);

  const res = await productAPI.getProduct();
  dispatch(productLoaded(res));
};

export default productReducer;

