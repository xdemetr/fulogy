import {applyMiddleware, combineReducers, createStore, compose} from 'redux';
import productReducer from './product-reducer';
import thunkMiddleware from 'redux-thunk';

let reducers = combineReducers({
  products: productReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducers,  composeEnhancers(applyMiddleware(thunkMiddleware)));

export default store;
